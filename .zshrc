# PROMPT='%f%F{38}lokdex%f%F{50}@%f%F{44}mbp%f%F{38}: %1~/%f %F{44}%#%f '

HISTSIZE=5000
HISTFILE=~/.zsh_history
SAVEHIST=5000
HISTDUP=erase
setopt appendhistory
setopt sharehistory
setopt incappendhistory
setopt hist_ignore_all_dups
setopt hist_save_no_dups
setopt hist_ignore_dups
setopt hist_find_no_dups

autoload -U compinit && compinit
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'

bindkey -e

export PATH="/usr/local/opt/node@16/bin:$PATH"

export PATH="/usr/local/opt/php@7.4/bin:$PATH"
export PATH="/usr/local/opt/php@7.4/sbin:$PATH"

export PATH="$HOME/.emacs.d/bin:$PATH"

export GOPATH=~/Projects/goWorkspace
export PATH=$PATH:$GOPATH/bin

export DB_ERP="postgres://postgres:postgres@localhost:5432/UCSMERP"
export SDKROOT=$(xcrun --show-sdk-path)

# Aliases for common functions
alias php-serve="php -S 127.0.0.1:8080"
alias cat="bat --paging=never"
alias ls="exa"
alias ol-ls="/bin/ls"

eval "$(starship init zsh)"

# Syntax highlighting - must install the package in the system
source /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
export PATH="/usr/local/opt/php@7.4/bin:$PATH"
export PATH="/usr/local/opt/php@7.4/sbin:$PATH"

# Added by Amplify CLI binary installer
export PATH="$HOME/.amplify/bin:$PATH"
export PATH="/usr/local/opt/ruby/bin:$PATH"
export PATH="$HOME/.gem/ruby/3.0.0/bin:$PATH"
